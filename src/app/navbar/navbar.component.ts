import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

 //Pour propager des informations entre le comosant père et fils 
 //on utilise les directives Input et Output

  @Input()
  showSideBar:boolean;

@Output()
showSideBarChange :EventEmitter<boolean>=new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }

  afficherSideBar(){
    this.showSideBar= !this.showSideBar;
    this.showSideBarChange.emit(this.showSideBar);

  }
}
